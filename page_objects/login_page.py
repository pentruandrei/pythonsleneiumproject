
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.base_page import BasePage


class LoginPage(BasePage):
    __url = "https://practicetestautomation.com/practice-test-login/"
    __username_field = (By.ID, "username")
    __password_field = (By.ID, "password")
    __submit_button = (By.XPATH, "//button[@class='btn']")
    __login_error_message = (By.ID, "error")

    def __init__(self, driver: WebDriver):
        super().__init__(driver)

    def open(self):
        # self._driver.get(self.__url)
        super()._open_url(self.__url)

    def execute_login(self, username: str, password: str):
        super()._type(self.__username_field, username)
        super()._type(self.__password_field, password)
        super()._click(self.__submit_button)

        # self._type(self.__username_filed, username)
        # self._type(self.__password_field, password)
        # self._click(self.__submit_button)

    def login_error_is_displayed(self) -> bool:
        return self.is_displayed(self.__login_error_message)

    @property
    def error_message(self) -> str:
        return super()._get_text(self.__login_error_message)
