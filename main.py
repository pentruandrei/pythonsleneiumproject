import time

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get("https://practicetestautomation.com/practice-test-login/")
# time.sleep(1)

"""

"""
username_locator = driver.find_element(By.ID, "username")
password_locator = driver.find_element(By.NAME, "password")
submit_locator = driver.find_element(By.XPATH, "//button[@id='submit']")
username_locator.send_keys("student")
password_locator.send_keys("Password123")
submit_locator.click()
time.sleep(0.5)
logged_in_text_locator = driver.find_element(By.TAG_NAME, "h1")
logged_in_text = logged_in_text_locator.text
assert logged_in_text == "Logged In Successfully"
# logout_button = driver.find_element(By.LINK_TEXT,"https://practicetestautomation.com/practice-test-login/")
logout_button = driver.find_element(By.LINK_TEXT, "Log out")
assert logout_button.is_displayed()
actual_url = driver.current_url
print(actual_url)
assert actual_url == "https://practicetestautomation.com/logged-in-successfully/"
driver.close()
