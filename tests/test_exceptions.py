import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestExceptions():
    item_name = "Shaworma"

    @pytest.mark.exceptions
    @pytest.mark.skip
    def test_no_such_element_exception(self, driver):
        driver.get("https://practicetestautomation.com/practice-test-exceptions/")
        edit_button = driver.find_element(By.ID, "edit_btn")
        text_box = driver.find_element(By.CLASS_NAME, "input-field")
        add_button = driver.find_element(By.ID, "add_btn")
        edit_button.click()
        text_box.clear()
        text_box.send_keys(item_name)
        add_button.click()
        confirmation_message = driver.find_element(By.ID, "confirmation")
        assert confirmation_message.get_attribute("innerText") == "Row 2 was added", ("Confirmation message was not "
                                                                                      "displayed")
        wait = WebDriverWait(driver, 6)
        row2 = wait.until(expected_conditions.presence_of_element_located((By.ID, "row2")))
        # row2 = driver.find_element(By.ID, "row2")
        row1 = driver.find_element(By.ID, "row1")
        listed_item = row1.find_element(By.TAG_NAME, "input").get_property("value")
        assert listed_item == self.item_name

    @pytest.mark.exceptions
    @pytest.mark.skip
    def test_element_not_interactable_exception(self, driver):
        driver.get("https://practicetestautomation.com/practice-test-exceptions/")
        add_btn = driver.find_element(By.ID, "add_btn")
        add_btn.click()
        wait = WebDriverWait(driver, 6)
        row2 = wait.until(expected_conditions.presence_of_element_located((By.ID, "row2")))
        text_box = row2.find_element(By.CLASS_NAME, "input-field")
        text_box.send_keys(self.item_name)


    @pytest.mark.exceptions
    def test_invalid_state_exception(self, driver):
        wait = WebDriverWait(driver, 2)

        driver.get("https://practicetestautomation.com/practice-test-exceptions/")
        edit_btn = driver.find_element(By.ID, "edit_btn")
        edit_btn.click()
        # input_field = driver.find_element(By.CLASS_NAME, "input-field")
        input_field = wait.until(expected_conditions.element_to_be_clickable((By.CLASS_NAME, "input-field")))
        input_field.clear()
        input_field.send_keys(self.item_name)
        save_btn = driver.find_element(By.ID, "save_btn")
        save_btn.click()
        text_value = input_field.get_attribute("value")
        assert text_value == self.item_name


    @pytest.mark.exceptions
    def test_stale_element_reference_exception(self, driver):
        wait = WebDriverWait(driver, 1)
        driver.get("https://practicetestautomation.com/practice-test-exceptions")
        instruction_message = driver.find_element(By.ID, "instructions")
        add_btn = driver.find_element(By.ID, "add_btn")
        add_btn.click()
        #this will throw stale exception
        # assert not instruction_message.is_displayed()
        wait.until(expected_conditions.invisibility_of_element(instruction_message))


