import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

from page_objects.logged_in_successfully import LoggedInSuccessfullyPage
from page_objects.login_page import LoginPage


class TestPositiveScenarios:
    @pytest.mark.login
    @pytest.mark.positive
    @pytest.mark.skip
    def test_positive_login(self, driver):
        driver.get("https://practicetestautomation.com/practice-test-login/")
        username_locator = driver.find_element(By.ID, "username")
        password_locator = driver.find_element(By.NAME, "password")
        submit_locator = driver.find_element(By.XPATH, "//button[@id='submit']")
        username_locator.send_keys("student")
        password_locator.send_keys("Password123")
        submit_locator.click()
        time.sleep(0.5)
        logged_in_text_locator = driver.find_element(By.TAG_NAME, "h1")
        logged_in_text = logged_in_text_locator.text
        assert logged_in_text == "Logged In Successfully"
        # logout_button = driver.find_element(By.LINK_TEXT,"https://practicetestautomation.com/practice-test-login/")
        logout_button = driver.find_element(By.LINK_TEXT, "Log out")
        assert logout_button.is_displayed()
        actual_url = driver.current_url
        print(actual_url)
        assert actual_url == "https://practicetestautomation.com/logged-in-successfully/"


    @pytest.mark.login
    @pytest.mark.positive
    def test_positive_login_with_page_objects(self, driver):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.execute_login("student", "Password123")
        logged_in_page = LoggedInSuccessfullyPage(driver)
        assert logged_in_page.header == "Logged In Successfully", "Expected login confirmation message is missing!"
        assert logged_in_page.current_url == logged_in_page.expected_url
        assert logged_in_page.is_log_out_button_displayed(), "Log out button is not displayed!"

# Type username student into Username field
# Type password Password123 into Password field
# Puch Submit button
# Verify new page URL contains practicetestautomation.com/logged-in-successfully/
# Verify new page contains expected text ('Congratulations' or 'successfully logged in')
# Verify button Log out is displayed on the new page



