import time
import pytest
from selenium.webdriver.common.by import By

from page_objects.login_page import LoginPage


class TestNegativeScenarios:

    @pytest.mark.login
    @pytest.mark.negative
    @pytest.mark.parametrize("username, password, error_message",
                             [("incorrect_username", "Password123", "Your username is invalid!"),
                              ("student", "incorrect_passowrd", "Your password is invalid!")])
    def test_negative_login(self, driver, username, password, error_message):
        login_page = LoginPage(driver)
        login_page.open()
        login_page.execute_login(username, password)
        # time.sleep(0.3)
        assert login_page.login_error_is_displayed()
        assert login_page.error_message == error_message, "Incorrect error message for login page"

        # error_message_locator = driver.find_element(By.ID, "error")
        # assert error_message_locator.is_displayed(), "Error message is not displayed when it should!"
        # assert error_message_locator.text == error_message, "Error message is incorrect"
