from selenium import webdriver
import pytest
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions


# @pytest.fixture(params=['chrome', 'firefox'])
@pytest.fixture
def driver(request):
    browser = request.config.getoption("--browser")
    if browser.lower() == "firefox":
        options = FirefoxOptions()
        options.add_argument("--headless")
        my_driver = webdriver.Firefox(options=options)
    elif browser.lower() == "chrome":
        options = ChromeOptions()
        options.add_argument("--headless")
        my_driver = webdriver.Chrome(options=options)
    # if request.param == "firefox":
    #     options = FirefoxOptions()
    #     options.add_argument("--headless")
    #     my_driver = webdriver.Firefox(options=options)
    # elif request.param == "chrome":
    #     options = ChromeOptions()
    #     options.add_argument("--headless")
    #     my_driver = webdriver.Chrome(options=options)
    else:
        raise TypeError("Expected 'firefox' or 'chrome'")
    my_driver.implicitly_wait(1)
    yield my_driver
    print("After yielding")
    my_driver.quit()


def pytest_addoption(parser):
    parser.addoption(
        "--browser", action="store", default="chrome", help="browser to execute the tests on (chrome or firefox)"
    )

#
# @pytest.fixture
# def browser_option(request):
#     return request.config.getoption("--browser")
