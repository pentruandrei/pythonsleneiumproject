import pytest


def add_two_numbers(x, y):
    return x + y

@pytest.mark.math
def test_small_numbers():
    assert add_two_numbers(1,2) == 3


@pytest.mark.math
def test_large_numbers():
    assert add_two_numbers(12312,1231) == 13543
# print(add_two_numbers())